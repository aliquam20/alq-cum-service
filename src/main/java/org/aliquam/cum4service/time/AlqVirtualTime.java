package org.aliquam.cum4service.time;

import lombok.Setter;
import org.aliquam.cum4service.session.RequireSession;
import org.aliquam.session.api.model.Contract;
import org.aliquam.session.api.model.CreateContractRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
public class AlqVirtualTime {

    @Setter
    private LocalDateTime time = null;

    public LocalDateTime getLocalDateTime() {
        if(time == null) {
            return LocalDateTime.now();
        }
        return time;
    }

    public LocalDate getLocalDate() {
        if(time == null) {
            return LocalDate.now();
        }
        return time.toLocalDate();
    }

}
