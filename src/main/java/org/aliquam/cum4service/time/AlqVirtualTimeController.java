package org.aliquam.cum4service.time;

import org.aliquam.cum4service.session.RequireSession;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AlqVirtualTimeController {

    private final AlqVirtualTime time;

    public AlqVirtualTimeController(AlqVirtualTime time) {
        this.time = time;
    }

    @PutMapping("/time")
    @RequireSession(internal = true)
    public void putTime(@Valid @RequestBody PutAlqVirtualTimeRequest newTime) {
        time.setTime(newTime.getTime());
    }

}
