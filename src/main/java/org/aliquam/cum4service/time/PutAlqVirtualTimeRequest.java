package org.aliquam.cum4service.time;

import lombok.Data;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class PutAlqVirtualTimeRequest {

    @NotNull
    private LocalDateTime time;
}
