package org.aliquam.cum4service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Use below class to import this library
 * \@Import(org.aliquam.cum4service.SpringBootConfigurationReference.class)
 */
@SuppressWarnings("SpringFacetCodeInspection")
@Configuration
@ComponentScan("org.aliquam.cum4service")
public class SpringBootConfigurationReference {
}
