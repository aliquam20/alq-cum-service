package org.aliquam.cum4service.session;

import org.aliquam.session.api.AlqSessionApi;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class AlqSessionManagerProvider {
    @Bean
    public AlqSessionManager provideAlqSessionManager() {
        return AlqSessionApi.provideAlqSessionManager();
    }
}
