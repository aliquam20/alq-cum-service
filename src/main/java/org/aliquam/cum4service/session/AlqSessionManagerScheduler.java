package org.aliquam.cum4service.session;

import lombok.extern.slf4j.Slf4j;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.aliquam.session.api.model.Session;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AlqSessionManagerScheduler {

    private final AlqSessionManager sessionManager;

    public AlqSessionManagerScheduler(AlqSessionManager sessionManager) {
        this.sessionManager = sessionManager;
        refresh();
    }

//    @Scheduled(fixedRateString = "PT1H")
    @Scheduled(fixedRateString = "PT5M")
    public void refresh() {
        try {
            log.info("Scheduled session refresh (if needed) start");
            sessionManager.getSession();
            log.info("Scheduled session refresh (if needed) end");
        } catch (Exception ex) {
            log.error("Scheduled session refresh failed", ex);
        }
    }

}
