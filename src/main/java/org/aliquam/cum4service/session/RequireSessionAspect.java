package org.aliquam.cum4service.session;

import org.aliquam.session.api.AlqSessionApi;
import org.aliquam.session.api.RequestAuthenticationException;
import org.aliquam.session.api.RequestAuthenticatorService;
import org.aliquam.session.api.connector.AlqSessionConnector;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

@Aspect
@Component
public class RequireSessionAspect {

    private final RequestAuthenticatorService requestAuthenticatorService;

    public RequireSessionAspect(AlqSessionManager sessionManager) {
        AlqSessionConnector connector = new AlqSessionConnector(sessionManager);
        requestAuthenticatorService = new RequestAuthenticatorService(connector::getSessionOrEmpty);
    }

    @Around("@annotation(org.aliquam.cum4service.session.RequireSession)")
    public Object handleRequireSession(ProceedingJoinPoint joinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        RequireSession requireSession = method.getAnnotation(RequireSession.class);

        String sessionHeader = request.getHeader(AlqSessionApi.SESSION_HEADER);
        checkSession(requestAuthenticatorService, sessionHeader, requireSession.internal());

        return joinPoint.proceed();
    }

    public static void checkSession(RequestAuthenticatorService authenticatorService, String sessionHeader, boolean isInternal) {
        try {
            authenticatorService.checkSession(sessionHeader, isInternal);
        } catch (RequestAuthenticationException e) {
            throw new ResponseStatusException(e.getErrorHttpStatus(), e.getMessage(), e);
        }
    }

}