package org.aliquam.cum4service.data;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
@Getter
@Setter
public class AlqBaseEntityWithUuidAndVersion implements Serializable, Comparable<AlqBaseEntityWithUuidAndVersion> {
    private static final long serialVersionUID = 1L;

    @Id
    private UUID id;

    @Version
    private Long version = null;

    @PrePersist
    public void generateIdentifierIfNeeded() {
        if(id == null) {
            id = UUID.randomUUID();
        }
    }

    public int hashCode() {
        return id.hashCode();
    }

    public boolean equals(Object other) {
        if(this == other) {
            return true;
        }
        if(other == null || getClass() != other.getClass()) {
            return false;
        }
        AlqBaseEntityWithUuidAndVersion otherEntity = (AlqBaseEntityWithUuidAndVersion) other;
        return id.equals(otherEntity.id);
    }

    public int compareTo(AlqBaseEntityWithUuidAndVersion other) {
        return id.compareTo(other.id);
    }

    public String toString() {
        return String.format("%s[%s]", this.getClass().getSimpleName(), this.getId());
    }
}
