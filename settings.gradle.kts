rootProject.name = "alq-cum-service"

pluginManagement {
    repositories {
        mavenLocal()
        maven("https://nexus.jeikobu.net/repository/maven-releases/")
        gradlePluginPortal()
    }
}