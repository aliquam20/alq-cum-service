import org.aliquam.AlqUtils

plugins {
    `maven-publish`
//    `java-library`
    id("org.springframework.boot") version "2.5.5"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("java")
    id("org.sonarqube") version "3.3"
    jacoco
    id("io.freefair.lombok") version "6.2.0"
    id("org.aliquam.alq-gradle-parent") version "0.4.14"
}
val alq = AlqUtils(project).withStandardProjectSetup()

group = "org.aliquam"
val artifactId = "alq-cum-service"
val baseVersion = "0.0.1"
version = alq.getSemVersion(baseVersion)

val branchName: String? = System.getenv("BRANCH_NAME")
val isJenkins = branchName != null
val dockerRegistryHost = if (isJenkins) alq.getEnvOrPropertyOrThrow("DOCKER_REGISTRY_HOST") else null
val dockerImageName = "aliquam/${artifactId}"

java.sourceCompatibility = JavaVersion.VERSION_11

dependencies {
    implementation("org.aliquam:alq-cum:0.0.1-DEV_BUILD")
    implementation("org.aliquam:alq-session-api:0.0.1-DEV_BUILD")

    implementation("javax.persistence:javax.persistence-api:2.2")

    implementation("org.springframework.boot:spring-boot-starter:2.5.5")
    implementation("org.springframework.boot:spring-boot-starter-actuator:2.5.5")
    implementation("org.springframework.boot:spring-boot-starter-web:2.5.5")
    implementation("org.springframework.boot:spring-boot-starter-validation:2.5.5")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.5.5")
    implementation("org.springframework.boot:spring-boot-starter-data-rest:2.5.5")
    implementation("org.springframework.boot:spring-boot-starter-aop:2.5.5")

    testImplementation("org.springframework.boot:spring-boot-starter-test:2.5.5")

    // Required to use Jersey HTTP client. More details in alq-cum project
    implementation("org.glassfish.jersey.ext:jersey-spring5:2.35")
}

sonarqube {
    if (isJenkins) {
        properties {
            property("sonar.projectKey", "aliquam20_${artifactId}")
            property("sonar.organization", "aliquam")
            property("sonar.host.url", "https://sonarcloud.io")
            property("sonar.branch.name", branchName!!)
            property("sonar.coverage.jacoco.xmlReportPaths", "$projectDir/build/reports/jacoco/test/jacocoTestReport.xml")
        }
    }
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        csv.isEnabled = false
    }
    dependsOn(tasks.test) // tests are required to run before generating the report
}

tasks.withType<Test> {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("myPublication") {
            groupId = "$group"
            artifactId = artifactId
            from(components["java"])
        }
    }
}
